/*-----( Import needed libraries )-----*/
#include <Stepper.h>

/*-----( Declare Constants, Pin Numbers )-----*/
//---( Number of steps per revolution of INTERNAL motor in 4-step mode )---
#define STEPS_PER_MOTOR_REVOLUTION 32

//---( Steps per OUTPUT SHAFT of gear reduction )---
#define STEPS_PER_OUTPUT_REVOLUTION 32 * 64  //2048

int speed = 700; // stepper motor speed
int steps = 100; // # of steps to move
int maxSeconds = 10;
int analogPin = A3;

//The pin connections need to be 4 pins connected
// to Motor Driver In1, In2, In3, In4  and then the pins entered
// here in the sequence 1-3-2-4 for proper sequencing
Stepper small_stepper(STEPS_PER_MOTOR_REVOLUTION, 8, 10, 9, 11);


void setup() {
  // initialize serial:
  Serial.begin(9600);
  randomSeed(analogRead(analogPin));
}

void loop() {
  moveStepper();
  long randNumber = random(maxSeconds); // Seconds Random upper limit
  Serial.println(randNumber);
  delay(randNumber * 1000);
}

void moveStepper() {
  small_stepper.setSpeed(speed);
  small_stepper.step(steps);
  delay(200);
  small_stepper.setSpeed(speed);
  small_stepper.step(-steps);
  
}
