#include <Servo.h> 

Servo myservo;
int pos = 90; // Speed control.  0 is fastest and 90 is the slowest
int pwmPin = 9;

int speed = 300; // servo motor speed

long maxSeconds = 180;
int analogPin = A3;

void setup() {
  // initialize serial:
  Serial.begin(9600);
  randomSeed(analogRead(analogPin));
}

void loop() {
  moveServo();
  long randNumber = random(maxSeconds); // Seconds Random upper limit
  Serial.println(randNumber);
  delay(randNumber * 1000);
}

void moveServo() {
  myservo.attach(pwmPin);
  myservo.writeMicroseconds(1700);
  delay(speed);
  myservo.detach();
  delay(300);
  myservo.attach(pwmPin);
  myservo.writeMicroseconds(1400);
  delay(speed);
  myservo.detach();
}
